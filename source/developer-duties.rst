.. _developer-duties:

Debian Developer's Duties
********************************************************************************************************************************

.. _package-maintainer-duties:

Package Maintainer's Duties
================================================================================================================================

As a package maintainer, you're supposed to provide high-quality
packages that are well integrated into the system and that adhere to the
Debian Policy.

.. _help-release:

Work towards the next ``stable`` release
--------------------------------------------------------------------------------------------------------------------------------

Providing high-quality packages in ``unstable`` is not enough; most
users will only benefit from your packages when they are released as
part of the next ``stable`` release. You are thus expected to
collaborate with the release team to ensure your packages get included.

More concretely, you should monitor whether your packages are migrating
to ``testing`` (see :ref:`testing`). When the migration doesn't
happen after the test period, you should analyze why and work towards
fixing this. It might mean fixing your package (in the case of
release-critical bugs or failures to build on some architecture) but it
can also mean updating (or fixing, or removing from ``testing``) other
packages to help complete a transition in which your package is
entangled due to its dependencies. The release team might provide you
some input on the current blockers of a given transition if you are not
able to identify them.

.. _maintain-stable:

Maintain packages in ``stable``
--------------------------------------------------------------------------------------------------------------------------------

Most of the package maintainer's work goes into providing updated
versions of packages in ``unstable``, but their job also entails taking
care of the packages in the current ``stable`` release.

While changes in ``stable`` are discouraged, they are possible. Whenever
a security problem is reported, you should collaborate with the security
team to provide a fixed version (see :ref:`bug-security`). When bugs
of severity important (or more) are reported against the ``stable``
version of your packages, you should consider providing a targeted fix.
You can ask the ``stable`` release team whether they would accept such
an update and then prepare a ``stable`` upload (see
:ref:`upload-stable`).

.. _rc-bugs:

Manage release-critical bugs
--------------------------------------------------------------------------------------------------------------------------------

Generally you should deal with bug reports on your packages as described
in :ref:`bug-handling`. However, there's a special category of bugs
that you need to take care of — the so-called release-critical bugs (RC
bugs). All bug reports that have severity ``critical``, ``grave`` or
``serious`` make the package unsuitable for inclusion in the next
``stable`` release. They can thus delay the Debian release (when they
affect a package in ``testing``) or block migrations to ``testing``
(when they only affect the package in ``unstable``). In the worst
scenario, they will lead to the package's removal. That's why these bugs
need to be corrected as quickly as possible.

If, for any reason, you aren't able fix an RC bug in a package of yours
within 2 weeks (for example due to time constraints, or because it's
difficult to fix), you should mention it clearly in the bug report and
you should tag the bug ``help`` to invite other volunteers to chime in.
Be aware that RC bugs are frequently the targets of Non-Maintainer
Uploads (see :ref:`nmu`) because they can block the ``testing``
migration of many packages.

Lack of attention to RC bugs is often interpreted by the QA team as a
sign that the maintainer has disappeared without properly orphaning
their package. The MIA team might also get involved, which could result
in your packages being orphaned (see :ref:`mia-qa`).

.. _upstream-coordination:

Coordination with upstream developers
--------------------------------------------------------------------------------------------------------------------------------

A big part of your job as Debian maintainer will be to stay in contact
with the upstream developers. Debian users will sometimes report bugs
that are not specific to Debian to our bug tracking system. These bug
reports should be forwarded to the upstream developers so that they can
be fixed in a future upstream release. Usually it is best if you can do
this, but alternatively, you may ask the bug submitter to do it.

While it's not your job to fix non-Debian specific bugs, you may freely
do so if you're able. When you make such fixes, be sure to pass them on
to the upstream maintainers as well. Debian users and developers will
sometimes submit patches to fix upstream bugs — you should evaluate and
forward these patches upstream.

In cases where a bug report is forwarded upstream, it may be helpful to
remember that the bts-link service can help with synchronizing states
between the upstream bug tracker and the Debian one.

If you need to modify the upstream sources in order to build a policy
compliant package, then you should propose a nice fix to the upstream
developers which can be included there, so that you won't have to modify
the sources of the next upstream version. Whatever changes you need,
always try not to fork from the upstream sources.

If you find that the upstream developers are or become hostile towards
Debian or the free software community, you may want to re-consider the
need to include the software in Debian. Sometimes the social cost to the
Debian community is not worth the benefits the software may bring.

Administrative Duties
================================================================================================================================

A project of the size of Debian relies on some administrative
infrastructure to keep track of everything. As a project member, you
have some duties to ensure everything keeps running smoothly.

.. _user-maint:

Maintaining your Debian information
--------------------------------------------------------------------------------------------------------------------------------

There's a LDAP database containing information about Debian developers
at https://db.debian.org/\ . You should enter your information
there and update it as it changes. Most notably, make sure that the
address where your debian.org email gets forwarded to is always up to
date, as well as the address where you get your debian-private
subscription if you choose to subscribe there.

For more information about the database, please see :ref:`devel-db`.

.. _key-maint:

Maintaining your public key
--------------------------------------------------------------------------------------------------------------------------------

Be very careful with your private keys. Do not place them on any public
servers or multiuser machines, such as the Debian servers (see
:ref:`server-machines`). Back your keys up; keep a copy offline.
Read the documentation that comes with your software; read the `PGP
FAQ <http://www.cam.ac.uk.pgp.net/pgpnet/pgp-faq/>`__ and `OpenPGP Best
Practices <https://riseup.net/en/security/message-security/openpgp/best-practices>`__.

You need to ensure not only that your key is secure against being
stolen, but also that it is secure against being lost. Generate and make
a copy (best also in paper form) of your revocation certificate; this is
needed if your key is lost.

If you add signatures to your public key, or add user identities, you
can update the Debian key ring by sending your key to the key server at
``keyring.debian.org``. Updates are processed at least once a month by
the ``debian-keyring`` package maintainers.

If you need to add a completely new key or remove an old key, you need
to get the new key signed by another developer. If the old key is
compromised or invalid, you also have to add the revocation certificate.
If there is no real reason for a new key, the Keyring Maintainers might
reject the new key. Details can be found at
https://keyring.debian.org/replacing_keys.html\ .

The same key extraction routines discussed in :ref:`registering`
apply.

You can find a more in-depth discussion of Debian key maintenance in the
documentation of the ``debian-keyring`` package and the
https://keyring.debian.org/ site.

Voting
--------------------------------------------------------------------------------------------------------------------------------

Even though Debian isn't really a democracy, we use a democratic process
to elect our leaders and to approve general resolutions. These
procedures are defined by the `Debian
Constitution <https://www.debian.org/devel/constitution>`__.

Other than the yearly leader election, votes are not routinely held, and
they are not undertaken lightly. Each proposal is first discussed on the
``debian-vote@lists.debian.org`` mailing list and it requires several
endorsements before the project secretary starts the voting procedure.

You don't have to track the pre-vote discussions, as the secretary will
issue several calls for votes on
``debian-devel-announce@lists.debian.org`` (and all developers are
expected to be subscribed to that list). Democracy doesn't work well if
people don't take part in the vote, which is why we encourage all
developers to vote. Voting is conducted via OpenPGP-signed/encrypted email
messages.

The list of all proposals (past and current) is available on the `Debian
Voting Information <https://www.debian.org/vote/>`__ page, along with
information on how to make, second and vote on proposals.

.. _inform-vacation:

Going on vacation gracefully
--------------------------------------------------------------------------------------------------------------------------------

It is common for developers to have periods of absence, whether those
are planned vacations or simply being buried in other work. The
important thing to notice is that other developers need to know that
you're on vacation so that they can do whatever is needed if a problem
occurs with your packages or other duties in the project.

Usually this means that other developers are allowed to NMU (see
:ref:`nmu`) your package if a big problem (release critical bug,
security update, etc.) occurs while you're on vacation. Sometimes it's
nothing as critical as that, but it's still appropriate to let others
know that you're unavailable.

In order to inform the other developers, there are two things that you
should do. First send a mail to ``debian-private@lists.debian.org`` with
[VAC] prepended to the subject of your message [1]_ and state the period
of time when you will be on vacation. You can also give some special
instructions on what to do if a problem occurs.

The other thing to do is to mark yourself as on vacation in the :ref:`devel-db` (this information is only
accessible to Debian developers). Don't forget to remove the on vacation
flag when you come back!

Ideally, you should sign up at the `OpenPGP coordination
pages <https://wiki.debian.org/Keysigning>`__ when booking a holiday and
check if anyone there is looking for signing. This is especially
important when people go to exotic places where we don't have any
developers yet but where there are people who are interested in
applying.

.. _s3.7:

Retiring
--------------------------------------------------------------------------------------------------------------------------------

If you choose to leave the Debian project, you should make sure you do
the following steps:

-  Orphan all your packages, as described in :ref:`orphaning`.

-  Remove yourself from uploaders for co- or team-maintained packages.

-  If you received mails via a @debian.org e-mail alias (e.g.
   press@debian.org) and would like to get removed, open a RT ticket for
   the Debian System Administrators. Just send an e-mail to
   ``admin@rt.debian.org`` with "Debian RT" somewhere in the subject
   stating from which aliases you'd like to get removed.

-  Please remember to also retire from teams, e.g. remove yourself from
   team wiki pages or salsa groups.

-  Use the link https://nm.debian.org/process/emeritus to log in to
   nm.debian.org, request emeritus status and write a goodbye
   message that will be automatically posted on debian-private.

   Authentication to the NM site requires an SSO browser certificate.
   You can generate them on https://sso.debian.org.

   In the case you run into problems opening the retirement process
   yourself, contact NM front desk using ``nm@debian.org``

It is important that the above process is followed, because finding
inactive developers and orphaning their packages takes significant time
and effort.

.. _returning:

Returning after retirement
--------------------------------------------------------------------------------------------------------------------------------

A retired developer's account is marked as "emeritus" when the process
in :ref:`s3.7` is followed, and "removed" otherwise. Retired
developers with an "emeritus" account can get their account re-activated
as follows:

-  Get access to an salsa account (either by remembering the
   credentials for your old guest account or by requesting a new one as
   described at `SSO Debian wiki page
   <https://wiki.debian.org/DebianSingleSignOn#If_you_ARE_NOT_.28yet.29_a_Debian_Developer>`__.

-  Mail ``nm@debian.org`` for further instructions.

-  Go through a shortened NM process (to ensure that the returning
   developer still knows important parts of P&P and T&S).

Retired developers with a "removed" account need to go through full NM
again.

.. [1]
   This is so that the message can be easily filtered by people who
   don't want to read vacation notices.
